import tensorflow as tf

import sys
sys.path.append("../lib/")

from ROOT import TH1F, TH2F, TCanvas
from Interface import *
from Kinematics import *
from Dynamics import *
from Optimisation import *

if __name__ == "__main__" : 

  md  = 1.869
  mpi = 0.139
  mk  = 0.497

  dlz = DalitzPhaseSpace(mpi, mk, mpi, md)

  mass   = Const(0.892)
  width  = Const(0.050)
  mass3  = FitParameter("mass3",  0.770, 0.6, 0.9, 0.01)
  width3 = FitParameter("width3", 0.150, 0.0, 0.5, 0.01)

  a1 = Complex(Const(1.0), Const(0.0))
  a2 = Complex(FitParameter("a2r",  -0.3, -10., 10., 0.01),  FitParameter("a2i", 0., -10., 10., 0.01) )
  a3 = Complex(FitParameter("a3r",   0.0, -10., 10., 0.01),  FitParameter("a3i", 2., -10., 10., 0.01) )

  dd = Const(5.)
  dr = Const(1.5)

  def model(x) : 
    ampl1 = a1*BreitWignerLineShape(dlz.M2ab(x), mass,  width,  mpi, mk, mpi, md, dr, dd, 1, 1)*HelicityAmplitude(dlz.CosHelicityAB(x), 1)
    ampl2 = a2*BreitWignerLineShape(dlz.M2bc(x), mass,  width,  mpi, mk, mpi, md, dr, dd, 1, 1)*HelicityAmplitude(dlz.CosHelicityBC(x), 1)
    ampl3 = a3*BreitWignerLineShape(dlz.M2ac(x), mass3, width3, mpi, mpi, mk, md, dr, dd, 1, 1)*HelicityAmplitude(dlz.CosHelicityAC(x), 1)
    return Density( ampl1 + ampl2 + ampl3 + Complex(Const(5.), Const(0.)))

  tf.set_random_seed(1)
  sess = tf.Session()
  init = tf.global_variables_initializer()
  sess.run(init)

  norm_sample = sess.run( dlz.RectangularGridSample(400, 400) )

  majorant = EstimateMaximum(sess, model(dlz.data_placeholder), dlz.data_placeholder, norm_sample)*1.5
  print "Maximum = ", majorant
  data_sample = RunToyMC( sess, model(dlz.data_placeholder), dlz.data_placeholder, dlz, 50000, majorant, chunk = 2000000)

  nll = UnbinnedNLL(model(dlz.data_placeholder), Integral( model(dlz.norm_placeholder) ) )
  RunMinuit(sess, nll, { dlz.data_placeholder : data_sample, dlz.norm_placeholder : norm_sample } )

  fit_sample = RunToyMC(sess, model(dlz.data_placeholder), dlz.data_placeholder, dlz, 200000, majorant, chunk = 2000000)

  h1 = TH2F("h1", "", 100, dlz.minab-0.2, dlz.maxab+0.2, 100, dlz.minbc-0.2 , dlz.maxbc+0.2 ) 
  h2 = TH2F("h2", "", 100, dlz.minab-0.2, dlz.maxab+0.2, 100, dlz.minbc-0.2 , dlz.maxbc+0.2 ) 
  h3 = TH1F("h3", "", 100, dlz.minab-0.2, dlz.maxab+0.2 ) 
  h4 = TH1F("h4", "", 100, dlz.minab-0.2, dlz.maxab+0.2 ) 
  h5 = TH1F("h5", "", 100, dlz.minbc-0.2, dlz.maxbc+0.2 ) 
  h6 = TH1F("h6", "", 100, dlz.minbc-0.2, dlz.maxbc+0.2 ) 

  for d in data_sample : 
    h1.Fill(d[0], d[1])
    h3.Fill(d[0])
    h5.Fill(d[1])

  for f in fit_sample : 
    h2.Fill(f[0], f[1])
    h4.Fill(f[0])
    h6.Fill(f[1])

  c = TCanvas("c","", 600, 600)
  c.Divide(2, 2)
  h4.SetLineColor(2)
  h6.SetLineColor(2)
  c.cd(1); h1.Draw("zcol")
  c.cd(2); h2.Draw("zcol")
  c.cd(3); h3.Draw("e"); h4.Scale(h3.Integral()/h4.Integral()); h4.Draw("h same")
  c.cd(4); h5.Draw("e"); h6.Scale(h5.Integral()/h6.Integral()); h6.Draw("h same")
  c.Update()
